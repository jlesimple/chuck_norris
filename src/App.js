import './stylesheets/App.css';

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

import React from 'react';

import Home from './components/Home';
import Joke from './components/Joke';
import NavBar from './components/NavBar';

class App extends React.Component{
  render () {
    return (
      <Router>
        <div className="App">
          <NavBar />
          <Routes>
            <Route path='/' element={< Home />}></Route>
            <Route path='/Joke' element={< Joke />}></Route>
          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;