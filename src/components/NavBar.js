import { Component } from 'react';

import { Link } from 'react-router-dom';

import Logo from './Logo';

export default class NavBar extends Component {
    render() {
        return (
            <header className="App-header">
                <nav>
                    <Logo />
                    <ul>
                        <li>
                           <Link to='/'>Home</Link>
                        </li>
                        <li>
                           <Link to='/Joke'>Joke</Link>
                        </li>
                    </ul>
                </nav>
          </header>
        )
    }
}