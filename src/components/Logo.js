import React from "react";
import logo from '../assets/logo.svg';

class Logo extends React.Component {
    render() {
        return (
            <img src={logo} className="App-logo" alt="logo" />
        )
    }
}

export default Logo;