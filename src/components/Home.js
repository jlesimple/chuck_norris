import React from "react";


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        }
    }

    thick() {
        this.setState({
            date : new Date()
        });
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.thick();
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }

    render() {
        return (
            <div className='home'>
            <p>Hello {this.props.name} !</p>
            <p>Il est {this.state.date.toUTCString()} et change toute le secondes </p>
            </div>
        )
    }
}

export default Home;