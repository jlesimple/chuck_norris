import { Component } from 'react';


export default class Joke extends Component {
    constructor(props) {
        super(props);
    

        this.apiURL ="https://api.chucknorris.io/jokes/random";

        this.state = {
            data: {
                value: null,
            }
        }
    }


  /*  componentDidMount() { // composent qui affiche une random joke
        fetch(this.apiURL)
            // fecth renvoie une promise. Lors de la résolution
            // de cette promise (en cas de succès), la fonction
            // donnée en parametre de then, a comme paramètre 
            // l'object représsentant la réponse HTTP.
            .then((response) => {
            // response.json()permet d'extarie de la réponse HTTP brute son coprs,
            // au format JSON. Cette fonction renvoie une promise, dons la résolution,
            // passée en paramètre then, a comme paramètre les données extraite. 
            return response.json()
        }).then(({ value }) => {
            this.setState({ data: { value } })
        })

    

        // regarder la méthode fetch
    } 
    */

    async componentDidMount() {
        const response = await fetch(this.apiURL);
        const { value } = await response.json();
        this.setState({data: { value }});
    }


    render() {
        return (
            <div className="chuck-norris-joke">
                <p>{this.state.data.value || 'loading ...'}</p>
            </div>
        )
    }
}